

# LIS 4368

## Zach Rodefer

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Distributed version control
    - Install Java Servlet

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Install MySQL
    - Create java servlet
    - Run Java servlet Apps

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create petstore database
    - forward engineer database
    - Run sql script updates to DB

4. [p1 README.md](p1/README.md "My p1 README.md file")
    - Create client-side data validation
    - Alter web app to match
    - Test data validation

5. [a4 README.md](a4/README.md "My a4 README.md file")
    - Create forms for customer table
    - Use MVC architecture
    - Server-side Data validation
	
6. [a5 README.md](a5/README.md "My a5 README.md file")
    - Create DBUtil class for resusable methods
    - Create connectionPool class to resuse connections
    - create CustomerDB class with prepared statements to transfer data to the database
	
7. [p2 README.md](p2/README.md "My p2 README.md file")
    - CRUD functionality
    - Data Tables
    - Update and delete functions