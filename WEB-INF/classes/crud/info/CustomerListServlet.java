// save as "<TOMCAT_HOME>\webapps\lis4368\WEB-INF\classes\crud\info\CustomerListServlet.java"
/*
1. Compile:
Windows:
  cd to C:\tomcat\webapps\lis4368\WEB-INF\classes
  javac -cp .;c:\tomcat\lib\servlet-api.jar crud/info/CustomerListServlet.java

Mac: 	
  cd to /Applications/tomcat/webapps/lis4368/WEB-INF/classes
  javac -cp .:/Applications/tomcat/lib/servlet-api.jar crud/info/CustomerListServlet.java

2. Run: http://localhost:9999/lis4368/customerList
*/
package crud.info;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
 
import crud.business.Customer;
import crud.data.CustomerDB;

//servlet CustomerList is mapped to the URL pattern /customerList. When accessing this servlet, it will return a message.
@WebServlet("/customerList")
public class CustomerListServlet extends HttpServlet
{
	//perform different request data processing depending on transfer method (here, Post or Get)
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
	{
		String url = "/index.jsp"; //initialize url value (used for logic below)
        
		// get current action
		String action = request.getParameter("action");
		if (action == null)
			{
				action = "join";  // default action
			}

        // perform action and set URL to appropriate page
		if (action.equals("join"))
			{
				url = "/customerform.jsp";    // the "join" page
			} 
		else if (action.equals("add"))
			{
				// get parameters from the request (data conversions not required here)
				//Reality-check: zip should be int, phone long, balance and totalSales BigDecimal data types
				String firstName = request.getParameter("fname");
				String lastName = request.getParameter("lname");
				String street = request.getParameter("street");
				String city = request.getParameter("city");
				String state = request.getParameter("state");
				String email = request.getParameter("email");
				String zip = request.getParameter("zip");
				String phone = request.getParameter("phone");
				String balance = request.getParameter("balance");
				String totalSales = request.getParameter("total_sales");
				String notes = request.getParameter("notes");

				String message; //display entry issues encountered to user
				
				// store data in Customer object: user
				Customer user = new Customer(firstName, lastName, street, city, state, email, zip, phone, balance, totalSales, notes);

				//here: check *only* for data entry
				//empty string: string with zero length.
				//null value: is unknown value--not having a string.
				
				//Reality-check: in production environment need rigorous data validation:
				//http://java-source.net/open-source/validation
				if (firstName == null || lastName == null || email == null || street == null || city== null || state == null || zip == null ||
					balance == null || totalSales == null || phone == null ||
						firstName.isEmpty() || lastName.isEmpty() || email.isEmpty() || street.isEmpty() || city.isEmpty() || state.isEmpty() || zip.isEmpty()
						||  balance.isEmpty() || totalSales.isEmpty() || phone.isEmpty())
					{
						message = "All text boxes required except Notes.";
						url = "/customerform.jsp";
					}
				else if (!firstName.matches("([A-Za-z\\-]+)") || firstName.length() > 15)
				{
					message = "First name can only contain letters and hyphens and no more than 15 characters.";
						url = "/customerform.jsp";
				}
				else if (!lastName.matches("([A-Za-z\\-]+)") || lastName.length() > 30)
				{
					message = "Last name can only contain letters and hyphens and no more than 30 characters";
						url = "/customerform.jsp";
				}
				else if (!email.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$") || email.length() > 100)
				{
					message = "Must include valid email and no more than 100 characters";
						url = "/customerform.jsp";
				}
				else if (!street.matches("([A-Za-z0-9,.\\s,\\-]+)") || street.length() > 30)
				{
					message = "Street can only contain letters, numbers, commas, hyphens, or periods and no more than 30 characters";
						url = "/customerform.jsp";
				}
				else if (!city.matches("([A-Za-z0-9,.\\s,\\-]+)") || city.length() > 30)
				{
					message = "City can only contain letters, numbers, hyphens, and space character and no more than 30 characters";
						url = "/customerform.jsp";
				}
				else if (!state.matches("([A-Za-z]+)") || state.length() != 2)
				{
					message = "State can only contain letters and must be 2 characters.";
						url = "/customerform.jsp";
				}
				else if (!zip.matches("([0-9]+)") || zip.length() > 9 || zip.length() < 5)
				{
					message = "Zip can only contain intergers and must be between 5 and 9 characters";
						url = "/customerform.jsp";
				}
				else if (!balance.matches("(\\d+(?:\\.\\d+)?)") || balance.length() > 6)
				{
					message = "Balance can only contain numbers and a decimal point and must be 6 or less characters";
						url = "/customerform.jsp";
				}
				else if (!totalSales.matches("(\\d+(?:\\.\\d+)?)") || totalSales.length() > 6)
				{
					message = "Balance can only contain numbers and a decimal point  and must be 6 or less characters";
						url = "/customerform.jsp";
				}
				else if (!phone.matches("([0-9]+)") || phone.length() != 10 )
				{
					message = "Phone can only contain numbers and must be 10 digits";
						url = "/customerform.jsp";
				}
				
				else
					{
						message = "";
						url = "/thanks.jsp";
						CustomerDB.insert(user);
					}
				request.setAttribute("user", user);
				request.setAttribute("message", message);
			}
		getServletContext()
			.getRequestDispatcher(url)
			.forward(request, response);
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException
	{
		doPost(request, response);
	}    
}
