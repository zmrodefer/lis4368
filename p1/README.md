
# LIS 4381

## Zach Rodefer

### Assignment Requirements:

    Project 1: 
    1: Create data client-side validation
    2: Alter online portfolio to match
    3: test application


#### README.md file should include the following items:


#### Assignment Screenshots:

*Screenshot of Splash screen*:

![Splash screen](img/img1.png)

*Screenshot of invalid data*:

![Invalid Data](img/img2.png)

*Screenshot of Valid Data*:

![Valid Data](img/img3.png)

