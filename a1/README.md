
# LIS 4381

## Zach Rodefer

### Assignment Requirements:

    Assignment 1: Distributed version contrl and JDK install
    1: Set up DCS
    2: Java servlet installation
    3: Run Hello.java 


#### README.md file should include the following items:


> #### Git commands w/short descriptions:

1. git init -Create an empty Git repository or reinitialize an existing one
2. git status -Show the working tree status
3. git add -Add file contents to the index
4. git commit -Record changes to the repository
5. git push -Fetch from and integrate with another repository or a local branch
6. git pull -Update remote refs along with associated objects
7. git branch -List, create, or delete branches


#### Assignment Screenshots:

*Screenshot of Hello world running*:

![AMPPS Installation Screenshot](img/jdk_install.png)

*Screenshot of Tomcat on localhost9999*:

![JDK Installation Screenshot](img/tomcat.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/zmr13FSUIT/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/zmr13FSUIT/myteamquotes/ "My Team Quotes Tutorial")
