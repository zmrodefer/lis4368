
# LIS 4381

## Zach Rodefer

### Assignment Requirements:

    Assignment 2: 
    1: Download and install MySQL
    2: Java servlet tutorial


#### README.md file should include the following items:


#### Assignment Screenshots:

*Screenshot of Query Results*:

![AMPPS Installation Screenshot](img/img1.png)

*Screenshot of SayHello.java*:

![AMPPS Installation Screenshot](img/img5.png)

*Screenshot of HelloHome.html*:

![AMPPS Installation Screenshot](img/img4.png)

*Screenshot of Directory level view*:

![AMPPS Installation Screenshot](img/img3.png)

*Screenshot of Selected Query*:

![AMPPS Installation Screenshot](img/img2.png)

*Screenshot of SayHi.java*:

![AMPPS Installation Screenshot](img/img6.png)
