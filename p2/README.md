
# LIS 4381

## Zach Rodefer

### Assignment Requirements:

    Project 2: 
    1: Create data table
    2: Modify servlet
    3: Create update and delete functions


#### README.md file should include the following items:


#### Assignment Screenshots:

*Screenshot of add customer*:

![Splash screen](img/img1.png)

*Screenshot of thanks.jsp*:

![Invalid Data](img/img2.png)

*Screenshot of display customer*:

![Valid Data](img/img3.png)

*Screenshot of update customer*:

![Valid Data](img/img4.png)

*Screenshot of delete customer*:

![Valid Data](img/img5.png)
