<%@page contentType="text/html" pageEncoding="utf-8"%>

<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Mark K. Jowett, Ph.D.">
	<link rel="icon" href="favicon.ico">

	<title>LIS4368 - JSP Forms</title>

	<%@ include file="/css/include_css.jsp" %>		
	
</head>
<body>

	<%@ include file="/global/nav_global.jsp" %>	

	<div class="container">
		<div class="starter-template">
			<div class="row">
				<div class="col-xs-12">
					
					<div class="page-header">
						<%@ include file="/global/header.jsp" %>
					</div>

    <h3>Thanks for joining our customer list!</h3>

    <p>Here is the information that you entered:</p>

		<div class="col-xs-12 col-sm-offset-4 text-left">
			
			<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
			
			<label>FName:</label> <c:out value="${user.fname}"/><br />
			<label>LName:</label> <c:out value="${user.lname}"/><br />
			<label>email:</label> <c:out value="${user.email}"/><br />
			<label>street:</label> <c:out value="${user.street}"/><br />
			<label>city:</label> <c:out value="${user.city}"/><br />
			<label>state:</label> <c:out value="${user.state}"/><br />
			<label>Zip:</label> <c:out value="${user.zip}"/><br />
			<label>phone:</label> <c:out value="${user.phone}"/><br />
			<label>balance:</label> <c:out value="${user.balance}"/><br />
			<label>total sales:</label> <c:out value="${user.totalSales}"/><br />
			<label>notes:</label> <c:out value="${user.notes}"/><br />

			<p>To enter another email address, <br />
				click the Back button in your browser or Return button below.</p>
							
			<form  method="post" class="form-horizontal" action="${pageContext.request.contextPath}/customerAdmin">
				<input type="hidden" name="thanks" value="join">
				<input type="submit" value="Return">
			</form>
			
		</div>

				</div>
			</div>

	<%@ include file="/global/footer.jsp" %>

	</div> <!-- end starter-template -->
 </div> <!-- end container -->

 	<%--@ include file="/js/include_js.jsp" --%>		 
		
</body>
</html>
