
# LIS 4381

## Zach Rodefer

### Assignment Requirements:

    Assignment 3: 
    1: Create petstore database
    2: forward engineer database
    3: Run sql script updates to DB


#### README.md file should include the following items:


#### Assignment Screenshots:

*Screenshot of Petstore Erd*:

![The erd model](img/erd.png)


1. [Petstore MWB file](a3.mwb "Petstore MWB file")
    
2. [A3 sql script](a3.sql "a3 sql script")
