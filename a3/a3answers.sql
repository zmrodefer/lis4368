--a3 sql
--1.
select --attr names
from store;

--2.
select sto_name, count(pet_id) as 'number of pets'
from petstore
    natural join pet
group by sto_id

--3.
select sto_id, cus_fname/lname, cus_balance
from petstore
    natural join pet
    natural join customer;
    
--4.
update customer
set cus_lname='Valens'
where cus_id=2;

--5.
delete from pet where pet_id=4;

--6.
